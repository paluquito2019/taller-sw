/**
 * @file Template.cpp
 * @author kevin palomino (paluquito2019@gmail.com)
 * @brief Excercise 02:
 Un maestro desea saber que porcentaje de hombres y que 
 porcentaje de mujeres hay en un grupo de estudiantes.
    
 * @version 1.0
 * @date 05.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	// Declaration and Initialization of variables
	int numberMen =0;
	int numberWomen=0;
	float porcentageMen=0.0;
	float porcentageWomen=0.0;
	
	// Ask data throught the terminal
	cout<<"Ingresar el numero de varones: ";
	cin>>numberMen;
	cout<<"Ingrese el numero de mujeres: ";
	cin>>numberWomen;
	
	// Calculation
	porcentageMen= ((float)numberMen/((float)numberMen+(float)numberWomen))*100;
	porcentageWomen= ((float)numberWomen/((float)numberMen+(float)numberWomen))*100;
	
	// Show results in terminal
	cout<<"\n\r El porcentaje de varones es: "<<porcentageMen<<"%";
	cout<<"\n\r El porcentaje de mujeres es: "<<porcentageWomen<<"%";
	
	return 0;
}




