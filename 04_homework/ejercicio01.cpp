/**
 * @file Template.cpp
 * @author kevin palomino (paluquito2019@gmail.com)
 * @brief Excercise 01:
    Realice los algoritmos de un cilindro ,calcular: el rea lateral y el volumen del cilindro.
	A = pi^2(radio)*altura	                V =radio^2(pi)*altura
	
 * @version 1.0
 * @date 05.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.14	


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	// Declaration and Initialization of variables
	float terminalInputHeight = 0.0; 		// Entrada de altura a traves del terminal
	float terminalInputRadius = 0.0;	 	// Entrada de radio a traves del terminal
	float lateralAreaCylinder = 0.0;		// Area lateral del cilindro calculada
	float volumeCylinder      = 0.0;		// Volumen del cilindro calculada
	
	// Ask data throught the terminal
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the radius of the cylinder: ";
	cin>>terminalInputRadius;
	cout<<"\tWrite the height of the cylinder: ";
	cin>>terminalInputHeight;
	
	// Calculation
	lateralAreaCylinder = 2.0 *PI*terminalInputRadius*terminalInputHeight;	// area del lateral = 2*pi*radio*h
	volumeCylinder = PI*pow(terminalInputRadius,2)*terminalInputHeight;		// valumen = pi^2(radio)*altura
	
	// Show results in terminal
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe lateral area is: "<<lateralAreaCylinder<<endl;
	cout<<"\tThe volume is: "<<volumeCylinder<<endl;
	
	return 0;
}




